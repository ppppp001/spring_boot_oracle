package com.tutorabc.fd.user.Controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"","v1"})
public class HealthController {

    @RequestMapping(method = RequestMethod.GET,value = "/check")
    public String check(){
        return "OK";
    }
}
