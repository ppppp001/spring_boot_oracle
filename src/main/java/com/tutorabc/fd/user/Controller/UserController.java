package com.tutorabc.fd.user.Controller;

import com.tutorabc.fd.user.Service.UserService;
import com.tutorabc.fd.user.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class UserController {
    @Autowired
    private UserService userService;



    private static final Logger logger = LogManager.getLogger(UserController.class);



    @RequestMapping(method = RequestMethod.POST,value = "/user")
    public void createUser(@RequestBody User user){
        userService.createUser(user);
    }


    @RequestMapping(method = RequestMethod.GET,value = "/user")
    public User getUser(){

        User rtn = new User();
        rtn.setUserId(System.currentTimeMillis());
        rtn.setUserName("test");
        logger.info("{}",rtn);
        return rtn;
    }

}
