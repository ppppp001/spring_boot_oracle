package com.tutorabc.fd.user.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender;



    @Value("${spring.mail.username}")
    private String userName;


    public Boolean sendMail(){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(userName);//发送方
        message.setTo("496013218@qq.com");//接收方
        message.setSubject("主题：简单邮件");//标题
        message.setText("测试邮件内容");//内容
        mailSender.send(message);
        return Boolean.TRUE;
    }
}
